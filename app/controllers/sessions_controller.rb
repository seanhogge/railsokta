class SessionsController < ApplicationController
  def create
	  @user = auth
	  session[:user] = auth.uid
	  session[:email] = auth.extra.raw_info.email
	  session[:name] = auth.extra.raw_info.name
	  session[:firstname] = auth.extra.raw_info.given_name
	  session[:lastname] = auth.extra.raw_info.family_name
	  session[:groups] = auth.extra.raw_info.groups
	  redirect_to root_path	
  end

  def destroy
	  session[:user] = nil
	  session[:email] = nil
	  session[:name] = nil
	  session[:firstname] = nil
	  session[:lastname] = nil
	  session[:groups] = nil
	  redirect_to root_path
  end

  protected

  def auth
	  request.env["omniauth.auth"]
  end
end
