Rails.application.routes.draw do
	root 'pages#home'
	get 'auth/:provider/callback', to: 'sessions#create'
	get 'login', to: redirect('/auth/okta'), as: 'login'
	get 'logout' => 'sessions#destroy'
end
