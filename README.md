# Simple Okta Auth

This is a skeleton app that is ready for Okta authentication via OAuth 2
provided by Omniauth's Okta strategy. By cloning this repo and editing two
files, the app is ready to use Okta for authentication, but can also use other
Omniauth strategies or custom-built strategies.

## Necessary Edits

The first is that 3 environment variable need to be set: `OKTA_ORG`,
`OKTA_CLIENT_ID` and `OKTA_CLIENT_SECRET`. This is OS-dependent, and a quick
Internet search should reveal the best method for setting these.

The second is somewhat optional. The provided strategy does not include Okta
groups in the information returned from Okta on a successful login. The
solution is either to edit the Omniauth strategy file (breaking the ability
upgrade the strategy via bundle) or to send an API request to get further information
after receiving the authorization info.

To edit the strategy, look for the file
~/.rvm/gems/ruby-[version]/gems/omniauth-okta[version]/lib/omniauth/strategies/okta.rb
and edit the DEFAULT_SCOPE variable by adding `groups` to the array. Below
that, there should be a section like:

```ruby
info do
	{
		name:       raw_info['name'],
		email:      raw_info['email'],
		first_name: raw_info['given_name'],
		last_name:  raw_info['family_name'],
		image:      raw_info['picture']
	}
end
```

and you'll need to add a single line:

```ruby
info do
	{
		name:       raw_info['name'],
		email:      raw_info['email'],
		first_name: raw_info['given_name'],
		last_name:  raw_info['family_name'], 
		image:      raw_info['picture'],
		groups: 	raw_info['groups'] 
	} 
end 
```

## Simplicity

This skeleton doesn't do anything other than set session variables to mark a
user as logged in, and sets them to nil to mark a user as logged out. This is
all done with some rudimentary code in the Sessions controller, mostly so
that it's easy to see what's happening. It can be made more complicated, or
more elegant at your discretion.
